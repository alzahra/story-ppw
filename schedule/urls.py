from django.urls import path
from . import views
from .views import index, addschedule, saveschedule

app_name = 'schedule'
urlpatterns = [
    path('', views.index, name='index'),
    path('addschedule/', views.addschedule, name='addschedule'),
    path('saveschedule/', views.saveschedule, name='saveschedule'),
    path('deleteschedule/', views.deleteschedule, name='deleteschedule')
]