from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ScheduleForm
from .models import Schedule

response = {}

def index(request):
    schedule = Schedule.objects.all().order_by('date', 'time')
    response['schedule'] = schedule
    html = 'schedule.html'
    print(response)
    return render(request, html, response)

def addschedule(request):
    response['form'] = ScheduleForm
    html = 'addschedule.html'
    return render(request, html, response)

def saveschedule(request):
    form = ScheduleForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['activity'] = request.POST['activity']
        response['category'] = request.POST['category']
        response['place'] = request.POST['place']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        schedule = Schedule(activity=response['activity'], category=response['category'], place=response['place'],
                            date=response['date'], time=response['time'])
        schedule.save()
        return HttpResponseRedirect('/../schedule')
    else:
        return HttpResponseRedirect('/../schedule/addschedule')

def deleteschedule(request):
    schedule = Schedule.objects.all().delete()
    response['schedule'] = schedule
    html = 'schedule.html'
    return HttpResponseRedirect('/../schedule')