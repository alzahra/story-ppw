from django.db import models

class Schedule(models.Model):
	activity = models.CharField(max_length=25)
	category = models.CharField(max_length=20)
	place = models.CharField(max_length=20)
	date = models.DateField()
	time = models.TimeField()