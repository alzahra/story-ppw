from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
    activity = forms.CharField(max_length=25, widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Please fill out activity name'}))
    category = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Please fill out category name'}))
    place = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Please fill out place name'}))
    date = forms.DateField(widget=forms.DateInput(attrs={'class' : 'form-control', 'type' : 'date'}))
    time = forms.TimeField(widget=forms.TimeInput(attrs={'class' : 'form-control', 'type' : 'time'}))

    class Meta:
        model = Schedule
        fields = ('activity','category', 'place', 'date', 'time')
        fiels_required = ('activity', 'category', 'place', 'date', 'time')